class BasketV1 {
  constructor() {
    this.items = [];
  }
  addItem(item) {
    this.items.push(item);
  }

  getCost() {
    const summaryPrice = this.items
      .map(item => item.price)
      .reduce((sum, itemPrice) => sum + itemPrice, 0);
    if (summaryPrice > 3000) {
      return summaryPrice * 0.9;
    }

    return summaryPrice;
  }

  clear() {
    this.items = [];
  }
}

module.exports = { BasketV1 };
