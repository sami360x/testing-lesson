class BasketV3 {
  constructor(discounter) {
    this.items = [];
    this.discounter = discounter;
  }

  addItem(item) {
    this.items.push(item);
  }

  async getCost() {
    const sumPrice = this.items
      .map(item => item.price)
      .reduce((sum, itemPrice) => sum + itemPrice, 0);

    if (!this.discounter) {
      throw new Error("provide discounter!");
    }

    return sumPrice - (await this.discounter.getDiscount(this.items));
  }

  clear() {
    this.items = [];
  }
}

module.exports = { BasketV3 };
