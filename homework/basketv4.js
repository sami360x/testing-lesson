class BasketV4 {
    constructor(discounter, userInfo) {
      this.items = [];
      this.discounter = discounter;
      this.userInfo = userInfo;
    }

    _validateItem(item) {
      if (!item.name) {
        throw new Error('товар должен иметь название')
      };

      if (!item.count) {
        throw new Error('товар должен иметь количество')
      };

      if (item.count <= 0) {
        throw new Error('количество товара не может быть меньше 1')
      }

      if (!item.price) {
        throw new Error('товар должен иметь цену')
      }

      if (!item.id) {
        throw new Error('товар должен иметь id')
      }
    }
  
    addItem(newItem) {
      this._validateItem(newItem);

      const existingItem = this.items.find(item => item.id === newItem.id)

      if (existingItem) {
        existingItem.count += newItem.count;
      } else {
        this.items.push(newItem);
      }
    }

    removeItem(itemToRemove) {
      this._validateItem(itemToRemove);

      const existingItem = this.items.find(item => item.id === itemToRemove.id)
      if (!existingItem) {
        throw new Error('нельзя удалить из корзины товар, который не был в неё добавлен');
      }

      if (itemToRemove.count > existingItem.count) {
        throw new Error('нельзя удалить из корзины товара больше, чем там есть');
      }

      if (itemToRemove.count === existingItem.count) {
        const itemToDeleteIndex = this.items.findIndex(item => item.id === itemToRemove.id)
        this.items.splice(itemToDeleteIndex, 1);
      } else {
        existingItem.count -= itemToRemove.count;
      }
    }

    getCost() {
      if (this.items.length === 0) {
        return 0;
      }


      const sumPrice = this.items
        .map(item => item.price * item.count)
        .reduce((sum, itemPrice) => sum + itemPrice, 0);
  
      if (!this.discounter) {
        throw new Error("provide discounter!");
      }
  
      const priceWithDiscount = sumPrice - this.discounter.getDiscount(this.items);
      if (priceWithDiscount < 0) {
        throw new Error('нельзя делать цену меньше 0')
      }

      return priceWithDiscount;
    }
  
    clear() {
      this.items = [];
    }
  }
  
  module.exports = { BasketV4 };
  