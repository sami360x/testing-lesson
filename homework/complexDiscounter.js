class ComplexDiscounter {
  constructor(userType, region) {
    this.userType = userType;
    this.region = region;
  }

  getDiscount(items) {
    let sumPrice = items
      .map(item => item.price * item.count)
      .reduce((sum, price) => sum + price, 0);
    let discount = 0;
    if (this.userType === "одмин") {
      return sumPrice;
    }

    if (
      this.region === "Новосибирск" ||
      this.region === "Алтайский край" ||
      this.regon === "Самара-городок"
    ) {
      discount += this._threeByPriceOfTwo(items);
      sumPrice -= discount;
    }

    if (this.region === "Новосибирск" && sumPrice > 3000 && sumPrice < 5000) {
      discount += sumPrice * 0.1;
    } else if (this.region === "Новосибирск" && sumPrice > 5000) {
      discount += sumPrice * 0.2;
    } else if (this.userType === "сотрудник") {
      discount += sumPrice * 0.1;
    }
    return discount
  }

  _threeByPriceOfTwo(items) {
    const discount = items.reduce((sum, item) => {
      const threes = Math.floor(item.count / 3);
      sum += threes * item.price;
      return sum;
    }, 0);
    return discount;
  }
}

module.exports = { ComplexDiscounter };
