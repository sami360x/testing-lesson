class CoronaVirusDiscounter {
  constructor() {
    this._restrictedItems = ["греча", "туалетная бумага", "маски"];
  }

  getDiscount(items) {
    if (
      items
        .filter(item => this._restrictedItems.includes(item.name))
        .some(item => item.count > 10)
    ) {
      throw new Error(
        "Во время эпидимологической ситуации в стране нельзя брать кризисных товаров больше 10 штук в одни руки"
      );
    }

    return 0;
  }
}

module.exports = { CoronaVirusDiscounter }